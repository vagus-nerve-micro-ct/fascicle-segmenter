# README #

Code to segment fascicle structures in micro-CT images of human nerve samples. 


### How do I get set up? ###

* We use the HPC cluster to run the software since they have machines with GPUs installed. The code can run as is if it is run on a GPU enabled machine with the required packages installed.
* Your PI needs to have an account with the HPC for you to use the compute resources. Email hpcsupport@case.edu for help with setup. Detailed help is [here](https://sites.google.com/a/case.edu/hpcc/), but a brief description is given here.
* You can clone this repository into your folder (under your case username) onto the HPC. Specifically, create a folder named fascicle-seg in your home folder on the HPC.
* Then this repository can be cloned using the git clone command in the fascicle-seg folder (for example /home/cxk340/fascicle-seg/).
* Go through the code files - data.py and network.py
* Current implementation uses the [segmentation_models library](https://github.com/qubvel/segmentation_models) - this makes using a few segmentation networks easier - requires installation in your HPC account
* Request a GPU node using a SSH client (like MobaXTerm). For example, the following command can be used:
```
srun --x11 -p gpu -C gpuk40 -N 1 -c 12 --gres=gpu:2 --mem=80g --time=150:00:00 --pty /bin/bash
```

* If part of Dr. Wilson's group, you can use the HPC node that is reserved for our group: 
```
srun --x11 -p cgpudlw -N 1 -c 12 --gres=gpu:2 --mem=100g --time=150:00:00 --pty /bin/bash
```

* Load the required libraries: 
```
module swap intel gcc
module load cuda/10.0 python python/3.6.6
```

* Run 
```
python unet.py
```

* Command line arguments: --train will allow you to run the network in prediction mode only if needed (--train 0). 


### Issues with set up ###
* The code is commented line by line, Keras has good tutorials - one [here](https://keras.io/). 
* Reach out to Chaitanya (cxk340@case.edu) if there are any issues.

* We could use this repository to ensure version control for the MATLAB code as well (maybe in a separate MATLAB folder).

### Things to do ###
* Current software trains a network on each nerve dataset, modify to train and test on multiple samples (requires inclusion of more samples in the data folder, test/predict code can be slightly rewritten to run the algorithm on samples in multiple folders).
* Try alternative segmentation networks, possibly 3D
* For some datasets, the registered images are substantially larger in size than (512, 512) creating a lot of blank and empty space. Write MATLAB code to save the location of the (512, 512) raw image in the registered image and crop/uncrop as needed.
* Add epineurium as an additional segmentation class - however this is difficult to segment in the the curent images
* Quantify human variability in segmentation - would be good to compare that with the algorithm's results in a manuscript
* Can we segment 1 in 25 images, interpolate labels between the slices and train the networks? The interpolated labels would not be perfect, his would provide a lot of training data easily.
* Segmentation borders are not "sharp" - investigate if a hard threshold will work well - pick a threshold from the AUC curve?

