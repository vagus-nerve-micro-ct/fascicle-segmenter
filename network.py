#Import modules
import os 
import numpy as np
import tensorflow as tf
import random as rn

# Things to reproduce results, although Keras on multiple GPU is not reproducible.
# The below is necessary for starting Numpy generated random numbers
# in a well-defined initial state.
np.random.seed(42)

# The below tf.set_random_seed() will make random number generation
# in the TensorFlow backend have a well-defined initial state.
# For further details, see: https://www.tensorflow.org/api_docs/python/tf/set_random_seed
tf.set_random_seed(1234)

# The below is necessary in Python 3.2.3 onwards to
# have reproducible behavior for certain hash-based operations.
# See these references for further details:
# https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
# https://github.com/fchollet/keras/issues/2280#issuecomment-306959926
os.environ['PYTHONHASHSEED'] = '0'

# The below is necessary for starting core Python generated random numbers
# in a well-defined state.
rn.seed(12345)

# Force TensorFlow to use single thread.
# Multiple threads are a potential source of
# non-reproducible results.
# For further details, see: https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)

# Import required libraries and modules
from keras.models import *
from keras.layers import Input, merge, Conv2D, MaxPooling2D, UpSampling2D, Dropout, Cropping2D, concatenate, BatchNormalization
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from keras.losses import categorical_crossentropy
from keras import backend as keras
from data import *
import argparse
from keras.utils import plot_model
import keras
from keras import backend as K

# This is a python library that makes testing multiple segmentation models easier
# Installation instruction are found here: https://github.com/qubvel/segmentation_models
# Alternatively, you can create/replicate networks using Keras commands (Conv2D, BatchNorm etc.)
# Segmentation models with pre-trained backbones
import segmentation_models as sm

K.set_session(sess)

# Weighted binary cross entropy function
def weighted_binary_crossentropy(y_true, y_pred):

    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)

    binary_crossentropy = K.binary_crossentropy(y_true_f, y_pred_f)

    # We add in the weights here, here we say that the foreground pixels are 9 times more important in our loss function than background
    weighted_vector = y_true_f * 0.92 + (1. - y_true_f) * 0.08
    weighted_binary_crossentropy_loss = weighted_vector * binary_crossentropy

    # Return mean of the loss function over batch
    return K.mean(weighted_binary_crossentropy_loss)

# Jaccard loss function (similar to Dice) - implementation from https://gist.github.com/wassname/f1452b748efcbeb4cb9b1d059dce6f96
def jaccard_loss(y_true, y_pred):
    smooth=100
    intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
    sum_ = K.sum(K.abs(y_true) + K.abs(y_pred), axis=-1)
    jac = (intersection + smooth) / (sum_ - intersection + smooth)

    return (1-jac)*smooth

class mynetwork(object):

    def __init__(self,args):

        # Copy command line arguments into object's variables
        self.user = args.u
        self.train = args.train
        self.weight_filename = args.w

        # Algorithm assumes training and testing images are of size (512, 512)
        # MATLAB code to convert the large registered images (sizes closer to 2k in each dimension) to (512, 512) and back is included in the bitbucket repo.
        train_img_h = 512
        train_img_w = 512

        test_img_h = 512
        test_img_w = 512

        self.mydata = dataProcess(train_img_h, train_img_w, self.user, test_img_h, test_img_w)

    def load_data(self):

        # Load training and test data for the network (some of these may be data generators if augmentation is applied)
        imgs_train, imgs_train_labels, imgs_valid, imgs_valid_labels, imgs_test = self.mydata.load_data()
        return imgs_train, imgs_train_labels, imgs_valid, imgs_valid_labels, imgs_test

    def train_and_test(self):

        # Get the neural network architecture
        print('Loading network architecture')
        # Feature Pyramid Network (FPN)   
        base_model = sm.FPN('vgg16', encoder_weights='imagenet', classes=1, activation='sigmoid')
        # Add a convolutional layer with 3 filters/channels so that an input of (512, 512, 1) can used
        inp = Input(shape=(512, 512, 1))
        l1 = Conv2D(3, (1,1))(inp)
        out = base_model(l1)
        # Complete the model
        model = Model(inp, out, name = base_model.name)
        # Print model summary
        model.summary()
        # Compile the model with a specified learning rate and loss function
        model.compile(optimizer = Adam(lr = 1e-6), loss = jaccard_loss,  metrics = ['accuracy'])
        print('Loaded network architecture \n')

        print('Loading training, validation and test data \n')
        imgs_train, imgs_train_labels, imgs_valid, imgs_valid_labels, imgs_test = self.load_data()
        print('Loaded training, validation and test data \n')

        if self.train == 1:
            print('Training.. \n')
            
            # Create a checkpoint to save the network weights to a file. Loss on the validation data set will be monitored
            model_checkpoint = ModelCheckpoint(self.weight_filename, monitor='val_loss', verbose=1, save_best_only=True)
            # Stop network training, use this in callbacks when training if required.
            early_stopping = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience = 5)

            # Fit the model to the training data and use validation data for checkpoints/early stopping
            model.fit_generator(zip(imgs_train, imgs_train_labels), steps_per_epoch=256, epochs=200, verbose=1, validation_data=zip(imgs_valid, imgs_valid_labels), validation_steps=256, callbacks=[model_checkpoint])

        # Predict on the test images
        print('Predicting..')
        model.load_weights(self.weight_filename)
        imgs_test_predictions = model.predict(imgs_test, batch_size=1, verbose=1)

        # Save predictions to the results folder
        print('Saving predictions on test images')
        self.mydata.save_test_predictions(imgs_test_predictions, self.user)

if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description='Deep learning based fascicle segmentation in micro-CT images')

    # Case username
    parser.add_argument('--u', default='cxk340', type=str,
                        help='Case username (example cxk340).')

    # Flag to perform training and testing or only testing
    parser.add_argument('--train', default=1, type=int,
                        help='Training performed')

    # Filename for storing network weights
    parser.add_argument('--w', default='fpn_tibial.hdf5', type=str,
    					help='Weights checkpoint filename')

    # Parses user arguments and stores them in the variable args
    args = parser.parse_args()

    # Make an object of myUnet class
    mynet = mynetwork(args)

    # Train and test the U-Net network as needed
    mynet.train_and_test()