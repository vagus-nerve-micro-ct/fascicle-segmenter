# Import necessary libraries
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import numpy as np 
import glob
import re
import os

def natural_keys(text):

    # Assumes that the file path of images is of the form /home/<case ID>/.../1.jpg
    # This fixes the image order to 1,2,3 instead of 1, 10, 100 etc.
    c = re.split('(/\d+)', text)
    return int(c[1].split('/')[1])

#Data processing class - takes care of data augmentation, reading images from files etc.
class dataProcess(object):

    # Assign values to variables local to each object of this class
    def __init__(self, img_height, img_width, username, test_img_h, test_img_w):
        self.img_height = img_height
        self.img_width = img_width
        self.username = username
        self.test_img_h = test_img_h
        self.test_img_w = test_img_w

    def load_data(self):

        # Augment the training images and labels, rotation, width and height shifts, zoom and flips
        datagen_args_image = dict(rotation_range=10,
                            width_shift_range=0.2,
                            height_shift_range=0.2,
                            shear_range=  0,
                            zoom_range=0.2,
                            horizontal_flip=True,
                            fill_mode='nearest',
                            rescale=1./255)

        datagen_args_label = dict(rotation_range=10,
                            width_shift_range=0.2,
                            height_shift_range=0.2,
                            shear_range=  0,
                            zoom_range=0.2,
                            horizontal_flip=True,
                            fill_mode='nearest',
                            rescale=1./255)
        
        # Create datagenerators
        image_datagen = ImageDataGenerator(**datagen_args_image)
        labels_datagen = ImageDataGenerator(**datagen_args_label)
        
        # Load training images and labels from the respective directories - the actual image files need to be in this folder under an additional subdirectory labeled "all"
        # Batch size is defined here, can be increased if you have a larger GPU memory. Images are automatically scaled to the specified size here, so best to crop images prior to this step (target_size parameter)
        image_generator = image_datagen.flow_from_directory('/home/' + self.username + '/fascicle-seg/data/tibial/images/', color_mode='grayscale', class_mode=None, seed=1, batch_size=2, target_size=(self.img_height, self.img_width))
        labels_generator = labels_datagen.flow_from_directory('/home/' + self.username + '/fascicle-seg/data/tibial/labels/', color_mode='grayscale', class_mode=None, seed=1, batch_size=2, target_size=(self.img_height, self.img_width))

        # Augment the validation images and labels
        validation_args_image = dict(rotation_range=10,
                            width_shift_range=0.2,
                            height_shift_range=0.2,
                            shear_range=  0,
                            zoom_range=0.2,
                            horizontal_flip=True,
                            fill_mode='nearest',
                            rescale=1./255)

        validation_args_label = dict(rotation_range=10,
                            width_shift_range=0.2,
                            height_shift_range=0.2,
                            shear_range=  0,
                            zoom_range=0.2,
                            horizontal_flip=True,
                            fill_mode='nearest',
                            rescale=1./255)
        
        validation_image_datagen = ImageDataGenerator(**validation_args_image)
        validation_label_datagen = ImageDataGenerator(**validation_args_label)
        
        # Load validation images and labels from the respective directories
        valid_image_generator = validation_image_datagen.flow_from_directory('/home/' + self.username + '/fascicle-seg/data/tibial/validation/images/', color_mode='grayscale', class_mode=None, seed=1, batch_size=2, target_size=(self.img_height, self.img_width))
        valid_labels_generator = validation_label_datagen.flow_from_directory('/home/' + self.username + '/fascicle-seg/data/tibial/validation/labels/', color_mode='grayscale', class_mode=None, seed=1, batch_size=2, target_size=(self.img_height, self.img_width))


        # For test we want to run the network on all the images from the volume (1 in 25 of these were used for training/validation - training:validation is usually 75:25)
        self.test_imgs_list = glob.glob('/home/' + self.username + '/fascicle-seg/sample_tibial_images/*.png')

        # Read test image files and load them into a numpy array, divide by 255 so that values are within 0 and 1
        imgs_test_stack = np.zeros((len(self.test_imgs_list), self.test_img_h, self.test_img_w))
        for i in np.arange(len(self.test_imgs_list)):
            imgs_test_stack[i,:,:] = img_to_array(load_img(self.test_imgs_list[i]))[:,:,1]/255
        
        # Print number of test images found
        print('%d test images found' %(len(self.test_imgs_list)))
        imgs_test_stack = np.expand_dims(imgs_test_stack, axis=-1)

        # Return generators and test volume
        return image_generator, labels_generator, valid_image_generator, valid_labels_generator, imgs_test_stack


    def save_test_predictions(self, imgs_test_predictions, username):

        # Directory to save the test predictions
        save_dir = '/home/' + username + '/fascicle-seg/sample_tibial_results/'

        # If such a directory does not exist, make one
        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        # Save the predictions one by one
        for i in np.arange(imgs_test_predictions.shape[0]):

            img_test_prediction = array_to_img(imgs_test_predictions[i,:,:])
            filename_start_index = self.test_imgs_list[i].rfind('/')

            img_test_prediction.save(save_dir + '/' +  self.test_imgs_list[i][filename_start_index+1:])

if __name__ == '__main__':

    mydata = dataProcess(512, 512, 'cxk340', 512, 512)